#!/bin/bash

curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
apt-get install -y nginx nodejs
ln -s /srv/audiobook-finder/env/proxy.conf /etc/nginx/conf.d/proxy.conf
rm /etc/nginx/sites-enabled/default
service nginx restart

sed -i 's/#force_color_prompt/force_color_prompt/g' /home/ubuntu/.bashrc
echo 'cd /srv/audiobook-finder' >> /home/ubuntu/.bashrc

ln -s /srv/audiobook-finder/env/attach_api.sh /usr/local/bin/attach_api
ln -s /srv/audiobook-finder/env/attach_web.sh /usr/local/bin/attach_web

chmod +x /usr/local/bin/attach_api
chmod +x /usr/local/bin/attach_web
