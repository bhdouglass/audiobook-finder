import Vue from 'vue';
import Router from 'vue-router';
import Search from '@/components/Search';
import Book from '@/components/Book';

Vue.use(Router);

export default new Router({
    routes: [
        {
            path: '/',
            name: 'search',
            component: Search,
        }, {
            path: '/book/:id',
            name: 'book',
            component: Book,
        },
    ],
});
