import axios from 'axios';


export default {
    search: (term) => {
        return axios.get(`/api/search?term=${term}`).then((res) => {
            return res.data.data;
        });
    },

    details: (id) => {
        return axios.get(`/api/details/${id}`).then((res) => {
            return res.data.data;
        });
    },
};
