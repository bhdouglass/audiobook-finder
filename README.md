# Audiobook Finder

[Search for audiobooks](http://audiobooks.bhdouglass.com/#/) from
[Gutenberg.or](http://www.gutenberg.org/browse/categories/1) and
[Archive.org](https://archive.org/details/librivoxaudio).

## Development

* Install [vagrant](http://vagrantup.com/)
* Install the docker compose vagrant plugin:
    * Run: `vagrant plugin install vagrant-docker-compose`
* Start vagrant:
    * Run: `vagrant up`
* Install NPM dependencies:
    * Run: `npm install`
* Attach to the docker container (if needed - from inside the vagrant VM):
    * Attach to the api container: `attach_api`
    * Attach to the web container: `attach_web`
* Update your system's hosts file:
    * Add `192.168.58.143 local.audiobooks.bhdouglass.com`
* Visit the site:
    * In your browser go to: `local.audiobooks.bhdouglass.com`

## License

Copyright (C) 2018 [Brian Douglass](http://bhdouglass.com/)

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
