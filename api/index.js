'strict mode';

const express = require('express');

// TODO store these in a database
const archiveorg = require('./archiveorg.json');
const gutenberg = require('./gutenberg.json');

let books = archiveorg.concat(gutenberg);

let app = express();

//Setup cors
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    next();
});

app.use(express.static(__dirname + '/../dist/'));

app.get('/api/search', (req, res) => {
    let term = req.query.term ? req.query.term : '';
    let results = [];

    if (term) {
        results = books.filter((book) => {
            return (book.title.toLowerCase().indexOf(term.toLowerCase()) >= 0);
        }).sort((a, b) => {
            if (a.title > b.title) {
                return 1;
            }
            else if (a.title < b.title) {
                return -1;
            }

            return 0;
        });
    }

    res.send({
        success: true,
        data: results.slice(0, 10),
    });
});

app.get('/api/details/:id', (req, res) => {
    res.send({
        success: true,
        data: books.filter((book) => {
            return (book.id == req.params.id);
        })[0],
    });
});

// TODO allow this to be configurable
app.listen(8081, '0.0.0.0');

// TODO schedule scraping
