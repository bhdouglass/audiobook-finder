'use strict';

const osmosis = require('osmosis');
const fs = require('fs');

let books = [];
let ids = [];

osmosis
    .get('http://www.gutenberg.org/browse/categories/1')
    .follow('.pgdbaudio a@href')
    .set({
        'id': '//*[contains(text(), "EBook-No.")]/following-sibling::td',
        'title': '*[itemprop="headline"]',
        'author': '*[rel="marcrel:aut"]',
        'url': 'link[rel="canonical"]@href',
        //'content': '//*[contains(text(), "Contents")]/following-sibling::td',
        'copyright': '*[property="dcterms:rights"]',
        'language': '*[itemprop="inLanguage"] td',
        'published': '*[itemprop="datePublished"]',
        'subject': ['*[property="dcterms:subject"] a'],
    })
    .data((data) => {
        data.id = `gutenberg_${data.id}`;
        data.source = 'Gutenberg';

        if (ids.indexOf(data.id) == -1) {
            books.push(data);
            ids.push(data.id);
        }
    })
    .log(console.log)
    .error(console.log)
    .done(() => {
        fs.writeFileSync(__dirname + '/../gutenberg.json', JSON.stringify(books));
    });
