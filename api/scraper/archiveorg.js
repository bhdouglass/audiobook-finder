'use strict';

const osmosis = require('osmosis');
const fs = require('fs');

let books = [];
let ids = [];

osmosis
    .get('https://archive.org/details/librivoxaudio')
    .paginate('.page-next')
    .follow('.item-ia div a')
    .set({
        'title': 'span[itemprop="name"]',
        'url': 'link[rel="canonical"]@href',
        'author': '//*[text()="by"]/following-sibling::span/span/a/text()',
        'license': '*[rel="license"]',
        'downloads': ['.download-pill@href'],
        'download_titles': ['.download-pill'],
    })
    .data((data) => {
        data.id = data.url.replace('https://archive.org/details/', 'archiveorg_');
        data.source = 'Archive.org';

        let downloads = [];
        data.downloads.forEach((url, index) => {
            downloads.push({
                url: `https://archive.org${url}`,
                title: data.download_titles[index].replace('download', '').trim(),
            });
        });

        delete data.download_titles;
        data.downloads = downloads;

        if (ids.indexOf(data.id) == -1) {
            books.push(data);
            ids.push(data.id);
        }
    })
    .log(console.log)
    .error(console.log)
    .done(() => {
        fs.writeFileSync(__dirname + '/../archiveorg.json', JSON.stringify(books));
    });
